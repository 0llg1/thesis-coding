function data = Recallcode (subject,speed,session)
%% basic Psychtoolbox stuff

    % constants
    SCREEN_WIDTH = 1024;
    SCREEN_HEIGHT = 768;
    if ~exist('speed','var')
        speed = 1;
    end
    
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow', screen, 0, [1 1 SCREEN_WIDTH SCREEN_HEIGHT]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    margin = SCREEN_WIDTH * .2; % left margin for drawing text
    wrap_at = 40; % number of characters allowed per line
    Screen('TextSize',windowPointer, 50); %text size
    Screen('TextFont',windowPointer, 'Arial'); %text font
    
    % files that will be saved by easyKeys
    test_name_counting = ['/Users/lgroat/Documents/Results/RECALLcounting_subj_session' num2str(subject)];
    test_name_recall = ['/Users/lgroat/Documents/Results/RECALLwords_subj_session' num2str(subject)];

    %% initializing easyType with initEasyType
           
     % duration for which the subject has to respond to the question
    eTrecallDuration = 45 * speed;
    recall_period_dur = 45 * speed;
    
    % duration for which the subject has to respond to the question
    eTcountingDuration = 45 * speed;
    
    eTcounting = initEasyType(test_name_counting, ...
                            'default_parent', windowPointer, ...
                            'font_size', 20, ...
                            'char_limit', [1 32], ...
                            'trigger_next', true, ...
                            'prompt_dur', eTcountingDuration, ...
                            'console', true);
                        
     eTrecall = initEasyType(test_name_recall, ...
                            'default_parent', windowPointer, ...
                            'font_size', 20, ...
                            'char_limit', [1 32], ...
                            'trigger_next', true, ...
                            'prompt_dur', eTrecallDuration, ...
                            'console', true);


    instructions = ['Welcome to the experiment!' ...
                    ' You will be presented with lists of common nouns' ...
                    'and it is your job to remember as many words as you can' ...
                    'from each list. You will be given 45 seconds at the end of each trial' ...
                    'to type out as many words from the list you can.' ...
                    ' Press the Return key to begin.'];
    
    %Display instructions for section
    Screen('FillRect', windowPointer, [0, 0, 0]);
    Screen('TextSize',windowPointer, 30);
    Screen('TextFont',windowPointer, 'Arial');
    Screen('TextStyle', windowPointer, 1);
    DrawFormattedText(windowPointer, instructions, margin,'center',[255 255 255],wrap_at);
    Screen(windowPointer, 'Flip');
    [timeWaited offsetTime timedOut] = waitForKeyboard('Return');
                        
    
                        
    %% test prepartion
    if session == 1
        start_list = 1;
    elseif session == 2
        start_list = 7;
    end
    
    %% easyKeys execution
    for list = start_list:start_list + 5
        
        % figure out which lists to use, then prepare list for looping
        switch list
            case {1,2,3}
                this_list = ['BodypartsList' num2str(list)];
                cond = 'related';
                cond_number = 1;
            case {4,5,6}
                this_list = ['VegetablesList' num2str(list-3)];
                cond = 'related';
                cond_number = 1;
            case {7,8,9,10,11,12}
                this_list = ['UnrelatedList' num2str(list-6)];
                cond = 'unrelated';
                cond_number = 2;
        end
               
        this_list_str = ['/Users/lgroat/thesis-coding/Stimuli For Tests/RecallTests/' this_list '.csv'];

  
        
        % insert loading code for this_list_str
            FID = fopen(this_list_str,'r');
            data = textscan(FID,'%q', 'Delimiter', ',');
            this_list_items = data{1};
            fclose(FID);

        for i = 1:length(this_list_items);
            
              % write text
                DrawFormattedText(windowPointer,[this_list_items{i}], 'center', 'center', [255 255 255],wrap_at);

                 % you need to flip before calling easyKeys. If you catch the
                 % output, you'll have an exact stimulus onset time that you can feed
                 % into easyKeys
                   Screen(windowPointer,'Flip');
                   WaitSecs(1);
                   
        end % item list is done  
        
          %% this is where the code for the distractor goes
         % count backwards by three

         %present instructions to indicate what they need to do for the
         %distractor
        instructions2 = [' Please count backwards by three from the number that is given to you.' ...
            'While counting type the numbers into the box provided.' ...
             ' Press the Return key to begin.' ];

        %Display instructions for section
        Screen('FillRect', windowPointer, [0, 0, 0]);
        Screen('TextSize',windowPointer, 30);
        Screen('TextFont',windowPointer, 'Arial');
        Screen('TextStyle', windowPointer, 1);
        DrawFormattedText(windowPointer, instructions2, margin,'center',[255 255 255],wrap_at);
        Screen(windowPointer, 'Flip');
        [timeWaited offsetTime timedOut] = waitForKeyboard('Return'); 
  
        %this will indicate the number that the subject starts with
        this_cresp = (randi(900) + 99);
        this_cresp_string = num2str(this_cresp);
                
        Screen('FillRect', windowPointer, [0, 0, 0]);
        Screen('TextSize',windowPointer, 30);
        Screen('TextFont',windowPointer, 'Arial');
        Screen('TextStyle', windowPointer, 1);
        DrawFormattedText(windowPointer, [this_cresp_string], 'center','center',[255 255 255],wrap_at);
        Screen(windowPointer, 'Flip');
        WaitSecs(3);

        %the duration of the recall period
        dis_recall_dur = 16 * speed;

        %show number and get information
        start = GetSecs();

        while (GetSecs() - start) < dis_recall_dur;
            %correct response
            this_cresp = (this_cresp - 3);
            this_cresp_string = num2str(this_cresp);
            correct_response = {this_cresp_string};

            %trial duration
            trial_duration = dis_recall_dur - (GetSecs() - start);

            % set the location of the input box to be just right of the
            % formatted question text with width of 200 pixels
            boxDim = [SCREEN_WIDTH * .1 + 250, SCREEN_HEIGHT * .1 + 200, SCREEN_WIDTH * .1 + 550, SCREEN_HEIGHT * .1 + 300];

            % you need to flip before calling easyType. If you catch the
            % output, you'll have an exact stimulus onset time that you can
            % feed into easyType
            onset = Screen(windowPointer, 'Flip');

            % get a response, logging both the stimulus and condition (make 
            % sure to catch the result)
            eTcounting = easyType(eTcounting, ...
                                'dim', boxDim, ...
                                'onset', onset, ...
                                'cresp', correct_response, ...
                                'trial_dur', trial_duration);

            % separate trials with a fixation screen
            DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
            Screen(windowPointer,'Flip');

            % wait for a moment after we show the fixation screen
            isilength = 0.5 * speed;
            WaitSecs(isilength);   
        end % distractor loop

        %% this is where free recall goes
        %% initializing easyType with initEasyType

        %Insert instructions for Association section
        instructions2 = [' What were the words presented in the list?'];

        %Display instructions for section
        Screen('FillRect', windowPointer, [0, 0, 0]);
        Screen('TextSize',windowPointer, 40);
        Screen('TextFont',windowPointer, 'Arial');
        Screen('TextStyle', windowPointer, 1);
        DrawFormattedText(windowPointer, instructions2, margin,'center',[255 255 255],wrap_at);
        Screen(windowPointer, 'Flip');
        WaitSecs(4);

        %responses which are deemed correct;
        start = GetSecs();

        %correct response when recalling the list
        correct_response = this_list_items;

        while (GetSecs() - start) < recall_period_dur

            %recall period duration
            recall_trial_dur = recall_period_dur - (GetSecs() - start);

            % set the location of the input box to be just right of the
            % formatted question text with width of 200 pixels
            boxDim = [SCREEN_WIDTH * .1 + 250, SCREEN_HEIGHT * .1 + 200, SCREEN_WIDTH * .1 + 550, SCREEN_HEIGHT * .1 + 300];

            % you need to flip before calling easyType. If you catch the
            % output, you'll have an exact stimulus onset time that you can
            % feed into easyType
            onset = Screen(windowPointer, 'Flip');

            % get a response, logging both the stimulus and condition (make 
            % sure to catch the result)
            eTrecall = easyType(eTrecall, ...
                                'dim', boxDim, ...
                                'onset', onset, ...
                                'stim', this_list, ...
                                'cond', cond, ...
                                'trial_dur', recall_trial_dur, ...
                                'cresp', correct_response);

            % separate trials with a fixation screen
            DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
            Screen(windowPointer,'Flip');

            % wait for a moment after we show the fixation screen
            isilength = 0.5 * speed;
            WaitSecs(isilength);   

            % here we remove any correct answers from the list of possible
            % correct answers going forwards
            this_response = eTrecall.trials.resp(end);
            correct_pos = find(strcmpi(correct_response, this_response)); % position of correct response if any in list
            if ~isempty(correct_pos)
                correct_response(correct_pos) = [];
            end
        end % free recall loop
        
            % separate trials with a break screen
            DrawFormattedText(windowPointer,'Break before next trial. Press Return key to begin.','center','center',[255 255 255]);
            Screen(windowPointer,'Flip');
            [timeWaited offsetTime timedOut] = waitForKeyboard('Return'); 
    end % list
    
 % remove the psychtoolbox screen and clear its various suppressions
  sca
  ShowCursor;
  ListenChar(0);
  
  data.recall = easyScore(eTrecall);
  data.counting = easyScore(eTcounting);
return
    