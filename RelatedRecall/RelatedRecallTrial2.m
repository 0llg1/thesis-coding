 % we need to have some questions to show to the subject
    % to do so, open up the CSV file for unrelated recall, and set each
    %trial to a corresponding data column
    rRecallFID = fopen('Unsworth (Related).csv');
    data = textscan(RecallFID, '%q%q%q%q%q%q', 'Delimiter', ',');
    trial1 = data{1};
    trial2 = data{2};
    trial3 = data{3};
    trial4 = data{4};
    trial5 = data{5};
    trial6 = data{6};
    fclose(RecallFID);
    
%%Trial one
for i = 1:length(trial2);
    
    %Draw text to display words on screen
    DrawFormattedText(windowPointer, [trial2{i}], margin, 'center', 'center', [255 255 255]);
    Screen(windowPointer, 'Flip');
    
    %Wait for a moment after each word is displayed
    WaitSecs(1);
    
    % separate trials with a fixation screen
     DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
     Screen(windowPointer,'Flip');
        
    % wait for a moment after we show the fixation screen
    isilength = 0.5;
    WaitSecs(isilength);
end

%show delay target instructions
Screen('DrawText', windowPointer, 'You will now be presented with a 3-digit number,', 250, 450, [255 255 255]);
Screen('DrawText', windowPointer, 'you will need to count backwards from it by threes', 250, 550, [255 255 255]);
Screen(windowPointer, 'Flip');
WaitSecs(3);

%Show delay number
Screen('DrawFormattedText', windowPointer, '846', 'center', 'center', [255 255 255]);
Screen(windowPointer, 'Flip');
WaitSecs(15);

%Start recall
Screen('DrawFormattedText', windowPointer, 'Begin recall!', 'center', 'center', [255 255 255]);
Screen(windowPointer, 'Flip');

%%insert recall phase!

return 
   