

%% basic Psychtoolbox stuff
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow',screen, 0, [1 1 1440 900]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;   
 
    
%%Display instructions
    Screen('FillRect', windowPointer, [0, 0, 0]);
    Screen('TextSize',windowPointer, 25);
    Screen('TextFont',windowPointer, 'Arial');
    Screen('TextStyle', windowPointer, 1);
    Screen('DrawText', windowPointer, 'Welcome to the experiment', 600, 250, [255 255 255]);
    Screen('DrawText', windowPointer, 'You will be presented with lists of common nouns', 350, 450, [255 255 255]);
    Screen('DrawText', windowPointer, 'and it is your job to remember as many words as you can', 350, 550, [255 255 255]);
    Screen('DrawText', windowPointer, 'from each list. You will be given 45 seconds at the end of each trial', 350, 650, [255 255 255]);
    Screen('DrawText', windowPointer, 'to type out as many words from the list you can.', 350, 750, [255 255 255]);
    Screen(windowPointer, 'Flip');
    WaitSecs(4);

    
%% test prepartion
    if session == 1
        start_list = 1;
    elseif session == 2
        start_list = 7;
    end
    
    %% easyKeys execution
    for list = start_list:start_list + 5
        
        % figure out which lists to use, then prepare list for looping
        switch list
            case {1,2,3}
                this_list = ['Bodyparts' num2str(list)];
            case {4,5,6}
                this_list = ['Vegetables' num2str(list-3)];
            case {7,8,9,10,11,12}
                this_list = ['Unrelated' num2str(list-6)];
        end
               
        this_list_str = ['/Users/lgroat/Documents/Stimuli For Tests/RecallTests' this_list '.txt'];
       
        % insert loading code for this_list_str
            fid = fopen(this_list_str,'r');
            data = textscan(fid,'%s');
            this_list_items = data{1};
        

        for i = 1:length(this_list_items);

            onset = Screen(windowPointer,'Flip');

            % get a response, logging both the stimulus and condition (make
            % sure to catch the result)
            mySurvey = easyKeys(mySurvey, 'onset', onset, 'stim', this_list_items{i});

            % separate trials with a fixation screen
            DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
            Screen(windowPointer,'Flip');

            % wait for a moment after we show the fixation screen
            isilength = 0.5;
            WaitSecs(isilength);
        end % item list is done
    end       
        
        
        
  %% this is where the code for the distractor goes
     % count backwards by three
        
     %present instructions to indicate what they need to do for the
     %distractor
        Screen('DrawText', windowPointer, 'You will now be shown a random three digit number,', 250, 400, [255 255 255]);
        Screen('DrawText', windowPointer, 'your job is to count backwards by threes from this number', 250, 450, [255 255 255]);
        Screen('DrawText', windowPointer, 'please type while counting, as your answers will be recorded.', 250, 500, [255 255 255]);
        Screen(windowPointer, 'Flip');
        WaitSecs(4);
            
    %this will indicate the number that the subject starts with
     this_cresp = randi(900) + 99;    
     this_cresp = this_cresp - 3;
     this_cresp_str = num2str(this_cresp);
        
    %the question that will be presented
     questions = 'What is the number - 3?';
        
    %the duration of the recall period
    dis_recall_dur = 15;
  
    %show number and get information
    start = GetSecs();
    
    while (GetSecs() - start) < dis_recall_dur
   
        % write question text
        [nx, ny, textBounds] = DrawFormattedText(windowPointer, questions{i}, margin, 'center', [255 255 255], wrap_at);
       
        % set the location of the input box to be just right of the
        % formatted question text with width of 200 pixels
        boxDim = [nx+10, ny, nx+400, textBounds(4)];
        
        % you need to flip before calling easyType. If you catch the
        % output, you'll have an exact stimulus onset time that you can
        % feed into easyType
        onset = Screen(windowPointer, 'Flip');
        
        % get a response, logging both the stimulus and condition (make 
        % sure to catch the result)
        mySurvey = easyType(mySurvey, ...
                            'dim', boxDim, ...
                            'onset', onset, ...
                            'stim', questions{i}, ...
                            'cresp', correct_response{i}, ...
                            'cond', conditions(i));
        
        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5;
        WaitSecs(isilength);   
        
        
    % here we remove any correct answers from the list of possible
    % correct answers going forwards
        correct_pos = find(strcmp(correct_response, this_response)); % position of correct response if any in list
        if ~isempty(correct_pos)
            correct_response(correct_pos) = [];
       end
    end
        
%% this is where free recall goes
%% initializing easyType with initEasyType

    % duration for which the subject has to respond to the question
    duration = 45;
    recall_period_dur = 45;
    
    mySurvey = initEasyType(test_name, ...
                            'default_parent', windowPointer, ...
                            'font_size', 14, ...
                            'trigger_next', true, ...
                            'prompt_dur', duration, ...
                            'files', false);
    
    % we need to ask the subject to recall the words in the list
    questions = {'What were the words in the list?'};
    
    %responses which are deemed correct;
    correct_response = {data{1}};
    
    start = GetSecs();
    
    while (GetSecs() - start) < recall_period_dur
   
        % write question text
        [nx, ny, textBounds] = DrawFormattedText(windowPointer, questions{i}, margin, 'center', [255 255 255], wrap_at);
       
        % set the location of the input box to be just right of the
        % formatted question text with width of 200 pixels
        boxDim = [nx+10, ny, nx+400, textBounds(4)];
        
        % you need to flip before calling easyType. If you catch the
        % output, you'll have an exact stimulus onset time that you can
        % feed into easyType
        onset = Screen(windowPointer, 'Flip');
        
        % get a response, logging both the stimulus and condition (make 
        % sure to catch the result)
        mySurvey = easyType(mySurvey, ...
                            'dim', boxDim, ...
                            'onset', onset, ...
                            'stim', questions{i}, ...
                            'cresp', correct_response{i}, ...
                            'cond', conditions(i));
        
        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5;
        WaitSecs(isilength);   
        
        
    % here we remove any correct answers from the list of possible
    % correct answers going forwards
        correct_pos = find(strcmp(correct_response, this_response)); % position of correct response if any in list
        if ~isempty(correct_pos)
            correct_response(correct_pos) = [];
       end
    end
    

    