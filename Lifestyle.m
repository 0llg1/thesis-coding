function data = Lifestyle(subject,speed)  
%% code needs to be split into two parts - one for the hormone module, and one for alcohol and exercise module

%% basic Psychtoolbox stuff

    % constants
    SCREEN_WIDTH = 1024;
    SCREEN_HEIGHT = 768;
    if ~exist('speed','var')
        speed = 1;
    end
    
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow', screen, 0, [1 1 SCREEN_WIDTH SCREEN_HEIGHT]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    margin = SCREEN_WIDTH * .2; % left margin for drawing text
    wrap_at = 40; % number of characters allowed per line
    
        % files that will be saved by easyKeys
        test_name = ['/Users/lgroat/Documents/Results/LIFESTYLE_subj' num2str(subject)];

      %% init easy keys
      % we will set duration to 5 so that the subject has up to 5s to respond
     % (you can set this to inf if you don't want any timeout at all).
       duration = inf * speed;

     % we will set triggerNext to true so that the screen disappears as 
     % soon as the subject responds.
        triggerNext = true;

     % now we will put it all together to initialize our test
        mySurvey_easyKeys = initEasyKeys(test_name, 'trigger_next', triggerNext, ...
                'prompt_dur', duration);
      %% init easyType
    % first, we will need a test name to identify the mySurveystruct and the 
    
    % duration for which the subject has to respond to the question
        duration = inf * speed;
    
    mySurvey_easyType = initEasyType(test_name, ...
                            'default_parent', windowPointer, ...
                            'font_size', 20, ...
                            'trigger_next', true, ...
                            'prompt_dur', duration, ...
                            'files', false);
    %% Hormones section
    %Insert instructions for Hormones section
    instructions = ['This section contains questions regarding your hormones, please respond to each as accurately as possible.' ...
        'Press the Return key when you are ready to begin.'];

        %Display instructions for section
        Screen('FillRect', windowPointer, [0, 0, 0]);
        Screen('TextSize',windowPointer, 25);
        Screen('TextFont',windowPointer, 'Arial');
        Screen('TextStyle', windowPointer, 1);
        DrawFormattedText(windowPointer, instructions, 'center','center',[255 255 255],wrap_at);
        Screen(windowPointer, 'Flip');
        [timeWaited offsetTime timedOut] = waitForKeyboard('Return');
    
    % gender
    mySurvey_easyKeys = presentQ(windowPointer, mySurvey_easyKeys, speed, 'Are you female?', {'y','n'}, [1 0], {'female','male'});
    
    % only evaluate rest of hormone module if female
    if mySurvey_easyKeys.trials.resp(1) == 1 % 1 -> female
        
        mySurvey_easyType = presentQ(windowPointer, mySurvey_easyType, speed, 'When was the date of your last menstrual period? (mm/dd/yyyy)', {}, [], {});
        
        mySurvey_easyKeys = presentQ(windowPointer, mySurvey_easyKeys, speed, 'How confident are you in this response?', {'1','2','3','4','5','6','7'}, [1 2 3 4 5 6 7], {'Not very', '..', '..', '..', '..', '..', 'Very'});
        
        mySurvey_easyType = presentQ(windowPointer, mySurvey_easyType, speed, 'When is the anticipated date of your next menstrual cycle? (mm/dd/yyyy)', {}, [], {});
        
        mySurvey_easyKeys = presentQ(windowPointer, mySurvey_easyKeys, speed, 'How regular are your cycles?', {'1','2','3','4','5','6','7'}, [1 2 3 4 5 6 7], {'Not very', '..', '..', '..', '..', '..', 'Very'});
        
        mySurvey_easyKeys = presentQ(windowPointer, mySurvey_easyKeys, speed, 'How confident are you in this response?', {'1','2','3','4','5','6','7'}, [1 2 3 4 5 6 7], {'Not very', '..', '..', '..', '..', '..', 'Very'});
        
        mySurvey_easyKeys = presentQ(windowPointer, mySurvey_easyKeys, speed, 'Do you use a form of contraception?', {'y', 'n'}, [1 0], {'yes', 'no'});
       
        %only evaluate if yes to contraception
        if mySurvey_easyKeys.trials.resp(5) == 1 % 1 --> yes
            
            mySurvey_easyKeys = presentQ(windowPointer, mySurvey_easyKeys, speed, 'Which type?', {'1','2','3','4','5'}, [1 2 3 4 5], {'Oral contraceptive (Birth control pill)', 'Intra Uterine Device', 'Hormonal injection of progestin', 'Vaginal ring', 'Birth control patch'});
            
                if mySurvey_easyKeys.trials.resp(6) == 1 % 1 == oral contraceptive
                    
                    mySurvey_easyType = presentQ(windowPointer, mySurvey_easyType, speed, 'Please name the brand of birth control you are using', {}, [], {});
                    
                    mySurvey_easyType = presentQ(windowPointer, mySurvey_easyType, speed, 'How long have you been using this type of contraceptive? (mm/dd/yyyy)', {}, [], {});
        
                    mySurvey_easyKeys = presentQ(windowPointer, mySurvey_easyKeys, speed, 'Have you ever taken a different type of contraception?', {'y', 'n'}, [1 0], {'yes', 'no'});
                        
                        %only evaluate if you have taken another type of
                        %contraceptive
                        if mySurvey_easyKeys.trials.resp(7) == 1 % 1 == yes
                            
                            mySurvey_easyKeys = presentQ(windowPointer, mySurvey_easyKeys, speed, 'Have you taken the morning after pill as a form of contraceptive?', {'y', 'n'}, [1 0], {'yes', 'no'});
                            
                                %only evaluate if you have used the morning
                                %after pill for contraceptive
                                if mySurvey_easyKeys.trials.resp(8) == 1 % 1 == yes
                                    
                                    mySurvey_easyType = presentQ(windowPointer, mySurvey_easyType, speed, 'How many days ago did you take the morning after pill?', {}, [], {});
                                    
                                    mySurvey_easyType = presentQ(windowPointer, mySurvey_easyType, speed, 'How many times have you used the morning after pill?', {}, [], {});
                                end
                        end
                end   
                
        end
end % female questions
  
  
 %% test prepartion for alcohol and exercise section

    %we need to open up a csv file and have the data read from there
    AEFID=fopen('AlchExer.csv');
    data = textscan(AEFID, '%q%q%q%q%q', 'Delimiter', ','); 
    questions = data{1};
    fclose(AEFID);

    
    %% easyKeys execution for alcohol and exercise sections
    
    %Insert instructions for BDI
    instructions = ['This section contains questions regarding your alcohol consumption and exercise patterns.' ...
        'Answer each question noting on these patterns within the past two weeks, on average. ' ...
        'Press the Return key when you are ready to begin.'];

    
        Screen('FillRect', windowPointer, [0, 0, 0]);
        Screen('TextSize',windowPointer, 25);
        Screen('TextFont',windowPointer, 'Arial');
        Screen('TextStyle', windowPointer, 1);
        DrawFormattedText(windowPointer, instructions, 'center','center',[255 255 255],wrap_at);
        Screen(windowPointer, 'Flip');
        [timeWaited offsetTime timedOut] = waitForKeyboard('Return');
    
    % loop over all questions
    for i = 1:length(questions);
        
        %we will allow different responses, others will be ignored
        allowed_keys = eval(data{2}{i});
        key_labels = eval(data{3}{i});
        key_values = eval(data{4}{i});
        
        % we want the computer to code these keyboard values into something we can
        % understand numerically, so we'll give each one a number value
        keymap = makeMap(key_labels, key_values, allowed_keys);
        
        % we will also provide a response guide for participants
        keyguide = [];   
        for j = 1:length(key_labels);
            keyguide = [keyguide key_labels{j} ': "' allowed_keys{j} '"\n'];
        end
        
        % write text
        DrawFormattedText(windowPointer,[questions{i} '\n\n' keyguide],margin,'center',[255 255 255],wrap_at);

        % you need to flip before calling easyKeys. If you catch the
        % output, you'll have an exact stimulus onset time that you can feed
        % into easyKeys
        onset = Screen(windowPointer,'Flip');

        % get a response, logging both the stimulus and condition (make
        % sure to catch the result)
        mySurvey_easyKeys = easyKeys(mySurvey_easyKeys, 'onset', onset, 'stim', questions{i}, 'keymap', keymap);

        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5 * speed;
        WaitSecs(isilength);

    end
 %% finish up and score the easyKeys test
    
    % log the time at which we broke free from the question loop: this is
    % the time at which our experiment ended
    experimentEndTime = GetSecs(); 
    
    % remove the psychtoolbox screen and clear its various suppressions
    sca
    ShowCursor;
    ListenChar(0);

    % calling easyScore with show_plot set to true will show you
    % some nice summary information, but be sure to catch the result: there
    % is good information in there that is not shown in the plot
    data.mySurvey_easyKeys = easyScore(mySurvey_easyKeys,experimentEndTime);
    data.mySurvey_easyType = easyScore(mySurvey_easyType,experimentEndTime);
return


function mySurvey = presentQ(windowPointer, mySurvey, speed, qText, qKeys, qValues, qLabels)

                      
        %we will allow different responses, others will be ignored
        allowed_keys = (qKeys);
        key_labels = (qLabels);
        key_values = (qValues);
        margin = 205; wrap_at = 40;
        
        % easyType question is signaled by empty key values
        if isempty(key_values)
            % write question text
            [nx, ny, textBounds] = DrawFormattedText(windowPointer, qText, margin, 'center', [255 255 255], wrap_at);

            
            % set the location of the input box to be just right of the
            % formatted question text with width of 200 pixels
            boxDim = [textBounds(1), textBounds(4)+20, textBounds(1)+400, textBounds(4)+50];

            % you need to flip before calling easyType. If you catch the
            % output, you'll have an exact stimulus onset time that you can
            % feed into easyType
            onset = Screen(windowPointer, 'Flip');

            % get a response, logging both the stimulus and condition (make 
            % sure to catch the result)
            mySurvey = easyType(mySurvey, ...
                                'cresp', {}, ...
                                'dim', boxDim, ...
                                'onset', onset, ...
                                'stim', qText);
            
        % otherwise easyKeys
        else
            % we want the computer to code these keyboard values into something we can
            % understand numerically, so we'll give each one a number value
            keymap = makeMap(key_labels, key_values, allowed_keys);

            % we will also provide a response guide for participants
            keyguide = [];   
            for j = 1:length(key_labels);
                keyguide = [keyguide key_labels{j} ': "' allowed_keys{j} '"\n'];
            end

            % write text
            DrawFormattedText(windowPointer,[qText '\n\n' keyguide],margin,'center',[255 255 255],wrap_at);

            % you need to flip before calling easyKeys. If you catch the
            % output, you'll have an exact stimulus onset time that you can feed
            % into easyKeys
            onset = Screen(windowPointer,'Flip');

            % get a response, logging both the stimulus and condition (make
            % sure to catch the result)
            mySurvey = easyKeys(mySurvey, 'onset', onset, 'stim', qText, 'keymap', keymap);

            % separate trials with a fixation screen
            DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
            Screen(windowPointer,'Flip');

            % wait for a moment after we show the fixation screen
            isilength = 0.5 * speed;
            WaitSecs(isilength);
        end % question type
return