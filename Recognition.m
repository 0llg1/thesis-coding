function mySurvey = Recognition(subject, speed)
 %% basic Psychtoolbox stuff

    % constants
    SCREEN_WIDTH = 1024;
    SCREEN_HEIGHT = 768;
   
    if ~exist('speed','var')
        speed = 1;
    end
    
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow', screen, 0, [1 1 SCREEN_WIDTH SCREEN_HEIGHT]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    margin = SCREEN_WIDTH * .2; % left margin for drawing text
    wrap_at = 40; % number of characters allowed per line
    Screen('TextSize',windowPointer, 50); %text size
    Screen('TextFont',windowPointer, 'Arial'); %text font
    
    %% initEasykeys for question
    
    % first, we will need a test name to identify the mySurveystruct and the 
    % files that will be saved by easyKeys
    test_name = ['/Users/lgroat/Documents/Results/RECOGNITIONObjects_subj' num2str(subject)'];
    
    % we will allow four possible respones and a skip button; other keys 
    % will be ignored. 
    allowed_keys = {'a','s', 'd'};
    key_labels= {'old','new', 'similar'};
    key_values = [1 2 3];
    
    % we want the computer to code these keyboard values into something we can
    % understand numerically, so we'll give each one a number value
    keymap = makeMap(key_labels, key_values,  allowed_keys);  

    % we will set duration to 5 so that the subject has up to 5s to respond
    % (you can set this to inf if you don't want any timeout at all).
    duration = 2.5 * speed;

    % we will set triggerNext to true so that the screen disappears as 
    % soon as the subject responds.
    triggerNext = false;
    
    % now we will put it all together to initialize our test
    % we won't use the stimmap feature in our demo
        mySurvey = initEasyKeys(test_name, 'keys', allowed_keys, ...
        'default_keymap', keymap, ...
        'trigger_next', triggerNext, ...
        'prompt_dur', duration);

%% in this section we generate lists for tests

% note: file names for objects: nums = 1-192, a/b, e.g., 001a.jpg
% note: in file for faces, nums = 1-105, -1/-2, e.g., 1-1.jpg (not consistent yet)

% these are hard-codings of the stimulus presentation orders for each block
order{1} = [56      7     17     60     81     93      5     41      2     80     76     91     18     74    108     51    100     14     70     85     16     75     78     87     50     15     64     69     30     65     31     52     84     28     86    106     46     21     44     61    101     92     22     34     49     95     48     67     98     77     94     88     40     33     10      1     38     82     37     39     35     19    105     68     13     90     29     66     25     32      8     55     62    104     42     53     63     36     27     72     59     83     54      9      3     97     71     57     11     47     99     79     12     89    103     58    102     73    107      6     20     24     45     23     26      4     43     96];
order{2} = [32     93     70    108     21     87     60     44     84      9      5     24     69     17     36     12     11     78     86     30     72      8     82     65    105     43    103     31     41     80    102     52     26     89     63     74     83     98     91     68     50     28     29     49     71     40     59    104      7     56     15     25    101     19     27     77    100     39     67     99     92     62     57     85     48     66     34     90     13     46     20      6     16     47     88     73     76      3      4     96     23     42      2      1     45     54     38     95    106     22     97     53     51     10     75     55     61     14     94     37     33     81     35     58     18     79    107     64];
order_face{1} = [  98  31  39  21  33  91  90  34   5  54   2  95  67  15  68  10 100  53 104  29  44  76  59  73  77  71  61  69 106  50  32   6  85 102  37  99  57  75  80  88  41  27  16  40  46  97  79  13  45  55  62 103  92   7  66  83  89  96  82  58 105  78   4  47 107  52  28  20  24  51 108  36  63  30  48  26  60  87  49  86  25  42  93  18 101  43  14  72  35  81  70  22  64   1   3  17  84  74  23  94  38  12   8  65  19  56   9  11];
order_face{2} = [14   65   66   43   61   37   10   81    3   18   56   12   51   39   57   27   31   26    6   67   24   77   40   75  107  102  108    4  105   62   38   41   54   20   25   73  103   87   48   96   34   63   78   50   93   53   19   36   55   42   22   85   11   86    2   71  104   84   47   46   88   89   35   95    8   72   30   99   60   29  101   33  100   79   92   70   68   76   69   74    1   15   83   80   52   91   21   94   16   90   64    7  106   32   17   28   98   49   44   23    5   82   97   58   45   59   13    9];

% assemble a complete list of filenames of all original and random items
num_objects = 192;
for i = 1:num_objects
    
    % pad small numbers with zeros
    if i < 10
        obj_name{i} = ['00' num2str(i)];
    elseif i < 100
        obj_name{i} = ['0' num2str(i)];
    else
        obj_name{i} = num2str(i);
    end
    
    % a and b indicate original and changed
    originals{i} = [obj_name{i} 'a.jpg'];
    changed{i} = [obj_name{i} 'b.jpg'];
    nums(i) = i;
end

% assign stimuli to blocks and conditions
num_blocks = 2;
target_counter = 1;
for block = 1:num_blocks
    list_counter = 1;
    
    % for each condition type we harvest items from the original list,
    % also keeping track of which number is which and whether the item is
    % similar or a repeat.
    for foils = 1:44
        list_items{block}{list_counter} = originals{target_counter};
        list_nums{block}(list_counter) = nums(target_counter);
        list_similar{block}(list_counter) = false;
        list_counter = list_counter + 1;
        target_counter = target_counter + 1;
    end
    for repeats = 1:16
        list_items{block}{list_counter} = originals{target_counter};
        list_nums{block}(list_counter) = nums(target_counter);
        list_similar{block}(list_counter) = false;
        list_counter = list_counter + 1;
        list_items{block}{list_counter} = originals{target_counter};
        list_nums{block}(list_counter) = nums(target_counter);
        list_similar{block}(list_counter) = false;
        list_counter = list_counter + 1;
        target_counter = target_counter + 1;
    end
    for similar = 1:16
        list_items{block}{list_counter} = originals{target_counter};
        list_nums{block}(list_counter) = nums(target_counter);
        list_similar{block}(list_counter) = true;
        list_counter = list_counter + 1;
        list_items{block}{list_counter} = changed{target_counter};
        list_nums{block}(list_counter) = nums(target_counter);
        list_similar{block}(list_counter) = true;
        list_counter = list_counter + 1;
        target_counter = target_counter + 1;
    end
    
    % the above allocation is done sequentially, so now we scramble it up.
    list_items{block} = list_items{block}(order{block});
    list_nums{block} = list_nums{block}(order{block});
    list_similar{block} = list_similar{block}(order{block});
    
    % assign condition to each item based on whether it's the first or
    % second time it appears in the sequence, and is/is not similar.
    aggregated = [];
    for i = 1:length(list_nums{block})
        if isempty(intersect(aggregated,list_nums{block}(i)))
            aggregated = [aggregated list_nums{block}(i)];
            cond{block}{i} = 'new';
        else
            if list_similar{block}(i)
                cond{block}{i} = 'similar';
            else
                cond{block}{i} = 'repeat';
            end
        end
    end
end


%% enter instructions for the test here

    
    instructions = ['In this experiment you will be shown a variety of the pictures. ' ...
        'For each picture, it is your job to state whether it is an old picture, a new picture or a similar picture. ' ...
        ' If it is an old picture press the ''a'' key, if it is a new picture press the ''s'' key, ' ...
        ' and if it is a similar picture press the ''d'' key. Press the Return button to begin. '];
  
  
  % Display instructions for section
    Screen('FillRect', windowPointer, [0, 0, 0]);
    Screen('TextSize',windowPointer, 30);
    Screen('TextFont',windowPointer, 'Arial');
    Screen('TextStyle', windowPointer, 1);
    DrawFormattedText(windowPointer, instructions, margin,'center',[255 255 255],wrap_at);
    Screen(windowPointer, 'Flip');
    [timeWaited, offsetTime, timedOut] = waitForKeyboard('Return');

     %% continuous recognition test

     % loop over blocks and trials
     for block = 1:num_blocks
        for trial = 1:length(list_items{block})
            % get the stimulus and condition for this trial
            this_stim = list_items{block}{trial};
            this_cond = cond{block}{trial};

             % set things up based on whether this is a old, new or similar
             % item
            if strcmp(this_cond, 'new');
                cresp = allowed_keys(2);
                conditions = 'new';
            elseif strcmp(this_cond, 'repeat');
                cresp = allowed_keys(1);
                conditions = 'repeat';
            else 
               cresp = allowed_keys(3);
               conditions = 'similar';
            end

            %Show pictures for stimulus
            this_picture = imread(list_items{block}{trial});
            Texture1 = Screen('MakeTexture', windowPointer, this_picture);
            Screen('DrawTexture', windowPointer, Texture1);

                    % you need to flip before calling easyKeys. If you catch the
                    % output, you'll have an exact stimulus onset time that you can feed
                    % into easyKeys
                    onset = Screen(windowPointer,'Flip');

                    % get a response, logging both the stimulus and condition (make
                    % sure to catch the result)
                    mySurvey = easyKeys(mySurvey, 'onset', onset, 'stim', list_items{block}{trial}, ...
                        'cond', conditions, 'cresp', cresp);

                    DrawFormattedText (windowPointer, '+', 'center', 'center', [255 255 255]);
                    Screen(windowPointer, 'Flip');
                    WaitSecs(.5 * speed);    


            % figure out cresp, show stimulus, get response
            % hint: if strcmp(this_cond,'new'), then ...
        end
        
            
    instructions2 = ['You have completed one block. Just a reminder that ' ...
        'if it is an old picture press the ''a'' key, if it is a new picture press the ''s'' key, ' ...
        ' and if it is a similar picture press the ''d'' key. ' ...
        'When you are ready to continue press the Return key to begin.'];
  
  
  % Display instructions for section
    Screen('FillRect', windowPointer, [0, 0, 0]);
    Screen('TextSize',windowPointer, 30);
    Screen('TextFont',windowPointer, 'Arial');
    Screen('TextStyle', windowPointer, 1);
    DrawFormattedText(windowPointer, instructions2, margin,'center',[255 255 255],wrap_at);
    Screen(windowPointer, 'Flip');
    [timeWaited, offsetTime, timedOut] = waitForKeyboard('Return');
     end
