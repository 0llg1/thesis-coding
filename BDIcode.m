function mySurvey = BDIcode(subject,speed)  
%% basic Psychtoolbox stuff

    % constants
    SCREEN_WIDTH = 1024;
    SCREEN_HEIGHT = 768;
    if ~exist('speed','var')
        speed = 1;
    end
    
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow', screen, 0, [1 1 SCREEN_WIDTH SCREEN_HEIGHT]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    margin = SCREEN_WIDTH * .2; % left margin for drawing text
    wrap_at = 50; % number of characters allowed per line
    
      
    
    %% initializing easyKeys with initEasyKeys
    
    % first we will get our test parameters ready. 
    
    % first, we will need a test name to identify the mySurveystruct and the 
    % files that will be saved by easyKeys
    test_name = ['/Users/lgroat/Documents/Results/BDI_subj' num2str(subject)];

    % we will set duration to 5 so that the subject has up to 5s to respond
    % (you can set this to inf if you don't want any timeout at all).
    duration = inf * speed;

    % we will set triggerNext to true so that the screen disappears as 
    % soon as the subject responds.
    triggerNext = true;
    
    % now we will put it all together to initialize our test
    mySurvey = initEasyKeys(test_name, 'trigger_next', triggerNext, ...
        'prompt_dur', duration);

    
    
    %% test prepartion

    %we need to open up a csv file and have the data read from there
    bdiFID=fopen('BDI.csv');
    data = textscan(bdiFID, '%q%q%q%q%q', 'Delimiter', ','); 
    questions = data{1};
    fclose(bdiFID);

    
    %% easyKeys execution
    
    %Insert instructions for BDI
    instructions = ['This questionnaire consists of 21 groups of statements.' ...
                    '  Please read each group of statements carefully, and then pick one in each ' ...
                    'group that best describes the way you have been feeling during the' ... 
                    'past two weeks, including today.  Ensure that you do not choose more than' ...
                    'one statement for any group, including Item 16 and Item 18.' ...
                       '  Press the Return key to begin.'];

    
        Screen('FillRect', windowPointer, [0, 0, 0]);
        Screen('TextSize',windowPointer, 25);
        Screen('TextFont',windowPointer, 'Arial');
        Screen('TextStyle', windowPointer, 1);
        DrawFormattedText(windowPointer, instructions, 'center' ,'center',[255 255 255],wrap_at);
        Screen(windowPointer, 'Flip');
        [timeWaited offsetTime timedOut] = waitForKeyboard('Return');
    
    % loop over all questions
    for i = 1:length(questions);
        
        %we will allow different responses, others will be ignored
        allowed_keys = eval(data{2}{i});
        key_labels = eval(data{3}{i});
        key_values = eval(data{5}{i});
        
        % we want the computer to code these keyboard values into something we can
        % understand numerically, so we'll give each one a number value
        keymap = makeMap(key_labels, key_values, allowed_keys);
        
        % we will also provide a response guide for participants
        keyguide = [];   
        for j = 1:length(key_labels);
            keyguide = [keyguide key_labels{j} ': "' allowed_keys{j} '"\n'];
        end
        
        % write text
        DrawFormattedText(windowPointer,[questions{i} '\n\n' keyguide],margin,'center',[255 255 255],wrap_at);

        % you need to flip before calling easyKeys. If you catch the
        % output, you'll have an exact stimulus onset time that you can feed
        % into easyKeys
        onset = Screen(windowPointer,'Flip');

        % get a response, logging both the stimulus and condition (make
        % sure to catch the result)
        mySurvey = easyKeys(mySurvey, 'onset', onset, 'stim', questions{i}, 'keymap', keymap);

        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5 * speed;
        WaitSecs(isilength);

    end
 %% finish up and score the easyKeys test
    
    % log the time at which we broke free from the question loop: this is
    % the time at which our experiment ended
    experimentEndTime = GetSecs(); 
    
    % remove the psychtoolbox screen and clear its various suppressions
    sca
    ShowCursor;
    ListenChar(0);

    % calling easyScore with show_plot set to true will show you
    % some nice summary information, but be sure to catch the result: there
    % is good information in there that is not shown in the plot
    mySurvey = easyScore(mySurvey,experimentEndTime);
return