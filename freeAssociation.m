function mySurvey = freeAssociation (subject,speed)
%% basic Psychtoolbox stuff

    % constants
    SCREEN_WIDTH = 1024;
    SCREEN_HEIGHT = 768;
    if ~exist('speed','var')
        speed = 1;
    end
    
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow', screen, 0, [1 1 SCREEN_WIDTH SCREEN_HEIGHT]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    margin = SCREEN_WIDTH * .2; % left margin for drawing text
    wrap_at = 40; % number of characters allowed per line
    Screen('TextSize',windowPointer, 30); %text size
    Screen('TextFont',windowPointer, 'Arial'); %text font
    
    % files that will be saved by easyKeys
    test_name = ['/Users/lgroat/Documents/Results/FREEASSOCIATION_subj' num2str(subject)];

    %% initializing easyType with initEasyType
    
    % duration for which the subject has to respond to the question
    duration = 60;
    
    mySurvey = initEasyType(test_name, ...
                            'default_parent', windowPointer, ...
                            'font_size', 18, ...
                            'char_limit', [3 32], ...
                            'trigger_next', true, ...
                            'prompt_dur', duration, ...
                            'console', true);
    
    %% test preparation
    
    %we need some words to show the subject
    faFID = fopen('FreeAssociation.csv');
    data = textscan(faFID, '%q', 'Delimiter', ',');
    words = data{1};
    fclose(faFID);

    instructions = ['Welcome to the experiment! You will be shown a list of words.' ...
        '  For each word write what comes to your mind when you see the word.'];
    
    %Display instructions for section
    Screen('FillRect', windowPointer, [0, 0, 0]);
    Screen('TextSize',windowPointer, 30);
    Screen('TextFont',windowPointer, 'Arial');
    Screen('TextStyle', windowPointer, 1);
    DrawFormattedText(windowPointer, instructions, margin,'center',[255 255 255],wrap_at);
    Screen(windowPointer, 'Flip');
    WaitSecs(4);

    %% easyType execution
  
 for i = 1:length(words);
    
     %Insert instructions for Association section
        instructions2 = [' Please write one word at a time. To type the next word, hit the Return key.' ...
        '  Your answers will be recorded based on what you type, so please ensure your spelling is correct.' ...
        '  Press the Return key to begin.'];

        %Display instructions for section
        Screen('FillRect', windowPointer, [0, 0, 0]);
        Screen('TextSize',windowPointer, 30);
        Screen('TextFont',windowPointer, 'Arial');
        Screen('TextStyle', windowPointer, 1);
        DrawFormattedText(windowPointer, instructions2, margin,'center',[255 255 255],wrap_at);
        Screen(windowPointer, 'Flip');
        [timeWaited offsetTime timedOut] = waitForKeyboard('Return');
          
     
  % write text
  DrawFormattedText(windowPointer,[words{i}],'center','center',[255 255 255],wrap_at);

  % you need to flip before calling easyKeys. If you catch the
  % output, you'll have an exact stimulus onset time that you can feed
  % into easyKeys
  Screen(windowPointer,'Flip');
  WaitSecs(2);
     
 %show box to type for free association
    association_dur = 60 * speed;
    start = GetSecs();
    
    while (GetSecs() - start) < association_dur
       correct_resp = {};
        %trial duration
        trial_duration = association_dur - (GetSecs() - start);
        
        % write question text
        %[nx, ny, textBounds] = DrawFormattedText(windowPointer, [], margin, 'center', [255 255 255], wrap_at);
       
        % set the location of the input box to be just right of the
        % formatted question text with width of 200 pixels
        boxDim = [SCREEN_WIDTH * .1 + 250, SCREEN_HEIGHT * .1 + 200, SCREEN_WIDTH * .1 + 550, SCREEN_HEIGHT * .1 + 300];
        
        % you need to flip before calling easyType. If you catch the
        % output, you'll have an exact stimulus onset time that you can
        % feed into easyType
        onset = Screen(windowPointer, 'Flip');
        
        % get a response, logging both the stimulus and condition (make 
        % sure to catch the result)
        mySurvey = easyType(mySurvey, ...
                            'dim', boxDim, ...
                            'onset', onset, ...
                            'cresp', correct_resp, ...
                            'trial_dur', trial_duration);
        
        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5 * speed;
        WaitSecs(isilength);   
    end
    
    %Ask the question regarding which word is correct
    % responses which are deemed correct
    
    questions{i} = 'Which word were you associating to?';
    correct_response = {words{i}};
    
     % write question text
      [nx, ny, textBounds] = DrawFormattedText(windowPointer, questions{i}, margin, 'center', [255 255 255], wrap_at);
       
        % set the location of the input box to be just right of the
        % formatted question text with width of 200 pixels
        boxDim = [textBounds(1), textBounds(4)+20, textBounds(1)+400, textBounds(4)+50];
        
        % you need to flip before calling easyType. If you catch the
        % output, you'll have an exact stimulus onset time that you can
        % feed into easyType
        onset = Screen(windowPointer, 'Flip');
        
        % get a response, logging both the stimulus and condition (make 
        % sure to catch the result)
        mySurvey = easyType(mySurvey, ...
                            'dim', boxDim, ...
                            'onset', onset, ...
                            'cresp', correct_response, ...
                            'stim', questions{i});
        
        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5 * speed;
        WaitSecs(isilength)
    
 end   
    % remove the psychtoolbox screen and clear its various suppressions
    sca
    ShowCursor;
    ListenChar(0);

return