clear all;

try
   commandwindow;
   pictures = {'Indoors1.jpg','Indoors2.jpg','Indoors3.jpg','Outdoors1.jpg','Outdoors2.jpg','Outdoors3.jpg'};
   nr=max(Screen('Screens'));
   [w, screenRect] = Screen('OpenWindow', nr, 0, [], 32, 2);
           
           

    %Present instructions
    Screen('FillRect', w, [0, 0, 0]);
    Screen('TextSize', w, 30);
    Screen('TextFont', w, 'Courier New');
    Screen('TextStyle', w, 1);
    Screen('DrawText', w, 'You will be presented with images', 600, 250, [0, 130, 150]);
    Screen('DrawText', w, 'Press the ''y'' key if you think the image is indoors', 400, 650, [0, 130, 150]);
    Screen('DrawText', w, 'and the ''n'' key if you think the image is not.', 400, 750, [0, 130, 150]);
    Screen('TextSize', w, 25);
    Screen('DrawText', w, '<Press any key to continue>', 700, 950, [0, 130, 150]);
    Screen(w, 'Flip');
    WaitSecs(6);
    

   for i = 1:length(pictures);
       ima=imread(pictures{i});
       Screen('PutImage', w, ima);
       onset = Screen(w,'Flip'); 
    isilength = 1;
        WaitSecs(isilength);
   end

sca;
  
end 
