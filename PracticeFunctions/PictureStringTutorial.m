  %% basic Psychtoolbox stuff
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow',screen, 0, [1 1 1440 900]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    Screen('TextSize',windowPointer,25); % set font size
    margin = 500; % left margin for drawing text
    wrap_at = 40; % number of characters allowed per line
    
    
    
    %% initializing easyKeys with initEasyKeys
    
    % first we will get our test parameters ready. 
    
    % first, we will need a test name to identify the mySurveystruct and the 
    % files that will be saved by easyKeys
    test_name = 'Demo';
    
    % we will allow four possible respones and a skip button; other keys 
    % will be ignored. 
    allowed_keys = {'y','n'};
    key_labels = {'yes', 'no'};
    key_values = [1 2];
    
    % we want the computer to code these keyboard values into something we can
    % understand numerically, so we'll give each one a number value
    keymap = makeMap(key_labels, key_values, allowed_keys);
    
    % Note that if you don't provide a default cresp to initEasyKeys, 
    % you will have to provide one for every trial when you call easyKeys.
    % Here we'll pass all keys except for the "skip" key as correct so that
    % accuracy will identify trials that received a valid response. If there
    % were no "correct" answer at all, we would just pass our "keys" argument
    % as a whole.
    default_cresp = allowed_keys(1:2);

    % we will set duration to 5 so that the subject has up to 5s to respond
    % (you can set this to inf if you don't want any timeout at all).
    duration = 5;

    % we will set triggerNext to true so that the screen disappears as 
    % soon as the subject responds.
    triggerNext = true;
    
    % we have two types of survey question that we will name here to make
    % analysis easier
    conditions = {'indoor','outdoor'};
    condition_numbers = [1 2];
    condmap = makeMap(conditions, condition_numbers);
    
    % we won't use this feature in our demo
    stimmap = {};
    
    % now we will put it all together to initialize our test
    mySurvey = initEasyKeys(test_name, 'keys', allowed_keys, ...
        'default_cresp', default_cresp, 'keymap', keymap, ...
        'trigger_next', triggerNext, ...
        'prompt_dur', duration);

    
    
    %% test prepartion

    % we need to have some questions to show to the subject
    

     pictures = {'Indoors1.jpg','Indoors2.jpg','Indoors3.jpg','Outdoors1.jpg','Outdoors2.jpg','Outdoors3.jpg'}; 
    % we will assign numerical labels based on the order of conditions
    % that we provided to "condmap"
    conditions = [1 1 1 2 2 2];

    % we will also provide a response guide for participants
    keyguide = [key_labels{1} ': "' allowed_keys{1} '"\n' ...
               key_labels{2} ': "' allowed_keys{2} '"'];
    

    
    %% easyKeys execution
Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
screen = 0; % show the front buffer on your main screen 
windowPointer = Screen('OpenWindow', 0, [900 900 1000], [1, 1, 1440, 900]);
Screen('FillRect', windowPointer, [0, 0, 0]);
Screen('TextSize',windowPointer, 30);
Screen('TextFont',windowPointer, 'Courier New');
Screen('TextStyle', windowPointer, 1);
Screen('DrawText', windowPointer, 'Welcome to the experiment', 500, 250, [255 255 255]);
Screen('DrawText', windowPointer, 'You will be presented with a variety of images.', 300, 450, [255 255 255]);
Screen('DrawText', windowPointer, 'Press the ''y'' key if you think the image is indoors', 300, 550, [255 255 255]);
Screen('DrawText', windowPointer, 'and the ''n'' key if the image is not.', 300, 650, [255 255 255]);
Screen(windowPointer, 'Flip');
WaitSecs(4);
    

    % loop over all questions
    for i = 1:length(pictures);

        %Draw pictures
       ima=imread(pictures{i});
       Screen('PutImage', windowPointer, ima);
       onset = Screen(windowPointer,'Flip'); 
       WaitSecs(2)

       % write text
        DrawFormattedText(windowPointer,['Is this picture taken indoors?' '\n\n' keyguide],margin,'center',[255 255 255],wrap_at);
        
        % you need to flip before calling easyKeys. If you catch the
        % output, you'll have an exact stimulus onset time that you can feed
        % into easyKeys
        Screen(windowPointer,'Flip');

       
        % get a response, logging both the stimulus and condition (make
        % sure to catch the result)
        mySurvey = easyKeys(mySurvey, 'onset', onset, 'stim', pictures{i},'cond', conditions(i));

        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5;
        WaitSecs(isilength);

    end
    
Screen('FillRect', windowPointer, [0, 0, 0]);
Screen('TextSize',windowPointer, 30);
Screen('TextFont',windowPointer, 'Courier New');
Screen('TextStyle', windowPointer, 1);
Screen('DrawText', windowPointer, 'Thank you for participating in our experiment', 350, 550, [255 255 255]);
Screen(windowPointer, 'Flip');
WaitSecs(2)
    %% finish up and score the easyKeys test
    
    % log the time at which we broke free from the question loop: this is
    % the time at which our experiment ended
    experimentEndTime = GetSecs(); 
    
    % remove the psychtoolbox screen and clear its various suppressions
    sca
    ShowCursor;
    ListenChar(0);

    % calling easyScore with show_plot set to true will show you
    % some nice summary information, but be sure to catch the result: there
    % is good information in there that is not shown in the plot!
    show_plot = true;
    mySurvey = easyScore(mySurvey,experimentEndTime,show_plot);

    % just a reminder about the files that were created
    disp(['easyKeys survey demo complete! Don''t forget to inspect the ' ...
        'files that were updated each time you called easyKeys to make ' ...
        'sure that nothing would be lost. They are ' myExperiment.fn_text ' and ' ...
        myExperiment.fn_matlab '.']);
sca;  
return