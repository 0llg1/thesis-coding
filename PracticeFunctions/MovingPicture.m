 % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    screen_size = get(0, 'ScreenSize');
    xCenter = screen_size(3)/2;
    yCenter = screen_size(4)/2;
    
    windowPointer = Screen('OpenWindow',screen, 0, [1 1 1440 900]); % Initialize sub-window
    
    position1 = [xCenter + 100, yCenter-200, xCenter + 649, yCenter+200]; %right
    position2 = [xCenter - 649, yCenter-200, xCenter - 100, yCenter + 200]; %left

 %open the picture into matlab and make a texture
 img = imread('Indoors1.jpg');
 Texture = Screen('MakeTexture', windowPointer, img);
 Screen('DrawTexture', windowPointer, Texture);
 Screen(windowPointer, 'Flip');
 WaitSecs(3);
 
 Screen('DrawTexture', windowPointer, Texture, [], position1);
 Screen(windowPointer, 'Flip')
 WaitSecs(3);
 
 Screen('DrawTexture', windowPointer, Texture, [], position2);
 Screen(windowPointer, 'Flip');
 WaitSecs(3);
 
 sca;