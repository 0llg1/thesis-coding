% syntax: result = demoEasyKeys()
%
% Call this function for a basic demonstration of how multiChoice works. It
% will administer a short series of survey questions and then score the
% result using easyKeys family functions. Be sure to read over the
% code to better understand what is going on. Returns the dataStruct
% created by easyKeys (also contained a in a file written by the
% function).
%
%
% Jordan Poppenk and Hanah Chapman, May 23, 2013 @ Princeton University

function mySurvey = demoEasyKeys()

    %% basic Psychtoolbox stuff

    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow',screen, 0, [1 1 1440 900]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    Screen('TextSize',windowPointer,25); % set font size
    margin = 20; % left margin for drawing text
    wrap_at = 40; % number of characters allowed per line
    
    
    
    %% initializing easyKeys with initEasyKeys
    
    % first we will get our test parameters ready. 
    
    % first, we will need a test name to identify the mySurveystruct and the 
    % files that will be saved by easyKeys
    test_name = 'survey question demo';
    
    % we will allow four possible respones and a skip button; other keys 
    % will be ignored. 
    allowed_keys = {'y','n','s','Return'};
    key_labels = {'yes', 'no', 'sort of','skip'};
    key_values = [1 2 3 -1];
    
    % we want the computer to code these keyboard values into something we can
    % understand numerically, so we'll give each one a number value
    keymap = makeMap(key_labels, key_values, allowed_keys);
    
    % Note that if you don't provide a default cresp to initEasyKeys, 
    % you will have to provide one for every trial when you call easyKeys.
    % Here we'll pass all keys except for the "skip" key as correct so that
    % accuracy will identify trials that received a valid response. If there
    % were no "correct" answer at all, we would just pass our "keys" argument
    % as a whole.
    default_cresp = allowed_keys(1:3);

    % we will set duration to 5 so that the subject has up to 5s to respond
    % (you can set this to inf if you don't want any timeout at all).
    duration = 6;

    % we will set triggerNext to true so that the screen disappears as 
    % soon as the subject responds.
    triggerNext = true;
    
    % we have two types of survey question that we will name here to make
    % analysis easier
    conditions = {'likes','academic'};
    condition_numbers = [1 2];
    condmap = makeMap(conditions, condition_numbers);
    
    % we won't use this feature in our demo
    stimmap = {};
    
    % now we will put it all together to initialize our test
    mySurvey = initEasyKeys(test_name, 'keys', allowed_keys, ...
        'default_cresp', default_cresp, 'keymap', keymap, ...
        'trigger_next', triggerNext, ...
        'prompt_dur', duration);

    
    
    %% test prepartion

    % we need to have some questions to show to the subject
    questions = {'Do you like chocolate?', 'Have you ever taken a history course?',... 
     'Have you ever taken a Psychology course?', 'Do you like swimming?', 'Do you like baseball?',...
     'Have you ever taken a math course?'};

    % we will assign numerical labels based on the order of conditions
    % that we provided to "condmap"
    conditions = [1 2 2 1 1 2];

    % we will also provide a response guide for participants
    keyguide = [key_labels{1} ': "' allowed_keys{1} '"\n' ...
               key_labels{2} ': "' allowed_keys{2} '"\n' ...
               key_labels{3} ': "' allowed_keys{3} '"\n' ...
               key_labels{4} ': "' allowed_keys{4} '"'];
    

    
    %% easyKeys execution
    
    % loop over all questions
    for i = 1:length(questions);

        % write text
        DrawFormattedText(windowPointer,[questions{i} '\n\n' keyguide],margin,'center',[255 255 255],wrap_at);

        % you need to flip before calling easyKeys. If you catch the
        % output, you'll have an exact stimulus onset time that you can feed
        % into easyKeys
        onset = Screen(windowPointer,'Flip');

        % get a response, logging both the stimulus and condition (make
        % sure to catch the result)
        mySurvey = easyKeys(mySurvey, 'onset', onset, 'stim', questions{i}, ...
            'cond', conditions(i));

        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5;
        WaitSecs(isilength);

    end

    
    
    %% finish up and score the easyKeys test
    
    % log the time at which we broke free from the question loop: this is
    % the time at which our experiment ended
    experimentEndTime = GetSecs(); 
    
    % remove the psychtoolbox screen and clear its various suppressions
    sca
    ShowCursor;
    ListenChar(0);

    % calling easyScore with show_plot set to true will show you
    % some nice summary information, but be sure to catch the result: there
    % is good information in there that is not shown in the plot!
    show_plot = true;
    mySurvey = easyScore(mySurvey,experimentEndTime,show_plot);

    % just a reminder about the files that were created
    disp(['easyKeys survey demo complete! Don''t forget to inspect the ' ...
        'files that were updated each time you called easyKeys to make ' ...
        'sure that nothing would be lost. They are ' myExperiment.fn_text ' and ' ...
        myExperiment.fn_matlab '.']);
    
return