function mySurvey = Imaging(subject,speed)  
%% basic Psychtoolbox stuff

    % constants
    SCREEN_WIDTH = 1024;
    SCREEN_HEIGHT = 768;
    if ~exist('speed','var')
        speed = 1;
    end
    
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow', screen, 0, [1 1 SCREEN_WIDTH SCREEN_HEIGHT]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    Screen('TextSize',windowPointer,25); % set font size
    margin = SCREEN_WIDTH * .2; % left margin for drawing text
    wrap_at = 30; % number of characters allowed per line
    

    %% initializing easyKeys with initEasyKeys
    
    % first we will get our test parameters ready. 
    
    % first, we will need a test name to identify the mySurveystruct and the 
    % files that will be saved by easyKeys
    test_name = ['/Users/lgroat/Documents/Imaging/Imaging_subj' num2str(subject)];
    
    % duration for which the subject has to respond to the question
    duration = 20 * speed;
    
    mySurvey = initEasyType(test_name, ...
                            'default_parent', windowPointer, ...
                            'font_size', 14, ...
                            'trigger_next', true, ...
                            'prompt_dur', duration, ...
                            'files', false);
    
    %% test preparation using EasyType
    
    % we need some questions for the subject
    imgFID = fopen('Imaging.csv');
    data = textscan(imgFID, '%q', 'Delimiter', ',');
    questions = data{1};
    fclose(imgFID);
            
    % assigning numerical labels based on the order of conditions that we
    % provided to condmap
    conditions = [];
            
    % responses which are deemed correct
    correct_response = {};
   
    %% easyType execution
    
    for i = 1:length(questions)
    
        % write question text
        [nx, ny, textBounds] = DrawFormattedText(windowPointer, questions{i}, margin, 'center', [255 255 255], wrap_at);
       
        % set the location of the input box to be just right of the
        % formatted question text with width of 200 pixels
        boxDim = [nx+ 10, ny+ 5, nx+400, textBounds(4)];
        
        % you need to flip before calling easyType. If you catch the
        % output, you'll have an exact stimulus onset time that you can
        % feed into easyType
        onset = Screen(windowPointer, 'Flip');
        
        % get a response, logging both the stimulus and condition (make 
        % sure to catch the result)
        mySurvey = easyType(mySurvey, ...
                            'cresp', {}, ...
                            'dim', boxDim, ...
                            'onset', onset, ...
                            'stim', questions{i});
        
        % separate trials with a fixation screen
        DrawFormattedText(windowPointer,'+','center','center',[255 255 255]);
        Screen(windowPointer,'Flip');
           
        % wait for a moment after we show the fixation screen
        isilength = 0.5 * speed;
        WaitSecs(isilength);

    end  

    
    % remove the psychtoolbox screen and clear its various suppressions
    sca
    ShowCursor;
    ListenChar(0);

return

