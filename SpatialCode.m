function data = SpatialCode(subject,speed)

 %% basic Psychtoolbox stuff

    % constants
    SCREEN_WIDTH = 1024;
    SCREEN_HEIGHT = 768;
    
    xc = SCREEN_WIDTH/2;
    yc = SCREEN_HEIGHT/2;
    
    if ~exist('speed','var')
        speed = 1;
    end
    
    % get screen ready
    Screen('Preference','SkipSyncTests', 2); %Skip the sync tests    
    screen = 0; % show the front buffer on your main screen
    windowPointer = Screen('OpenWindow', screen, 0, [1 1 SCREEN_WIDTH SCREEN_HEIGHT]); % Initialize sub-window
    
    %Suppress text output to console (so you don't write into whatever you have
    %open in the editor). Don't forget to turn it back on at the end with
    %ListenChar(0). Also suppress the mouse cursor.
    ListenChar(2);
    HideCursor;

    % use this for drawing text later
    margin = SCREEN_WIDTH * .2; % left margin for drawing text
    wrap_at = 40; % number of characters allowed per line
    Screen('TextSize',windowPointer, 50); %text size
    Screen('TextFont',windowPointer, 'Arial'); %text font

    %% initEasykeys eKleftness, eKrecognition, eKconfidence
    %% eKleftness
    
    % first, we will need a test name to identify the mySurveystruct and the 
    % files that will be saved by easyKeys
    test_name_eKleftness = ['/Users/lgroat/Documents/Results/SPATIALLeftness_subj_session' num2str(subject)'];
    
    % we will allow four possible respones and a skip button; other keys 
    % will be ignored. 
    allowed_keys_eKleftness = {'a','s'};
    key_labels_eKleftness = {'old Left','old Right'};
    key_values_eKleftness = [1 2];
    
    % we want the computer to code these keyboard values into something we can
    % understand numerically, so we'll give each one a number value
    keymap_eKleftness = makeMap(key_labels_eKleftness, key_values_eKleftness,  allowed_keys_eKleftness);  

    % we will set duration to 5 so that the subject has up to 5s to respond
    % (you can set this to inf if you don't want any timeout at all).
    duration_eKleftness = 2.5 * speed;

    % we will set triggerNext to true so that the screen disappears as 
    % soon as the subject responds.
    triggerNext = false;
    
    % now we will put it all together to initialize our test
    % we won't use the stimmap feature in our demo
    eKleftness = initEasyKeys(test_name_eKleftness, 'keys', allowed_keys_eKleftness, ...
        'default_keymap', keymap_eKleftness, ...
        'trigger_next', triggerNext, ...
        'prompt_dur', duration_eKleftness);

    %% eKrecognition
    
    % first, we will need a test name to identify the mySurveystruct and the 
    % files that will be saved by easyKeys
    test_name_eKrecognition = ['/Users/lgroat/Documents/Results/SPATIALRecognition_subj_session' num2str(subject)'];
    
    % we will allow four possible respones and a skip button; other keys 
    % will be ignored. 
    allowed_keys_eKrecognition = {'a','s', 'd'};
    key_labels_eKrecognition = {'old Left','old Right', 'new'};
    key_values_eKrecognition = [1 2 3];
    
    % we want the computer to code these keyboard values into something we can
    % understand numerically, so we'll give each one a number value
    keymap_recognition = makeMap(key_labels_eKrecognition, key_values_eKrecognition, allowed_keys_eKrecognition);  

    % we will set duration to 5 so that the subject has up to 5s to respond
    % (you can set this to inf if you don't want any timeout at all).
    duration_eKrecognition = 2.5 * speed;

    % we will set triggerNext to true so that the screen disappears as 
    % soon as the subject responds.
    triggerNext = false;
    
    % we have two types of survey question that we will name here to make
    % analysis easier
    conditions_eKrecognition = {'oldLeft', 'oldRight', 'new'};
    condition_numbers = [1 2 3];
    condmap = makeMap(conditions_eKrecognition, condition_numbers);
    
    % now we will put it all together to initialize our test
    % we won't use the stimmap feature in our demo
    eKrecognition = initEasyKeys(test_name_eKrecognition, 'keys', allowed_keys_eKrecognition, ...
         'default_keymap', keymap_recognition, ...
        'condmap', condmap, 'trigger_next', triggerNext, ...
        'prompt_dur', duration_eKrecognition);
    
    %% eKconfidence
    
    % first, we will need a test name to identify the mySurveystruct and the 
    % files that will be saved by easyKeys
    test_name_eKconfidence = ['/Users/lgroat/Documents/Results/SPATIALConfident_subj_session' num2str(subject)'];
    
    % we will set duration to 5 so that the subject has up to 5s to respond
    % (you can set this to inf if you don't want any timeout at all).
    duration_eKconfidence = inf * speed;

    % we will set triggerNext to true so that the screen disappears as 
    % soon as the subject responds.
    triggerNext = true;
    
    % we have two types of survey question that we will name here to make
    % analysis easier
    conditions_conf = {'sure', 'unsure'};
    condition_numbers_conf = [1 2];
    condmap_conf = makeMap(conditions_conf, condition_numbers_conf);
    
   
    % now we will put it all together to initialize our test
    % we won't use the stimmap feature in our demo
    eKconfidence = initEasyKeys(test_name_eKconfidence, ...
        'condmap', condmap_conf, 'trigger_next', triggerNext, ...
        'prompt_dur', duration_eKconfidence);
    
  %% display the instructions on the screen    
    
    
    instructions = ['In this experiment you will be shown a variety of the pictures to the left and ' ...
        'to the right of the center point of the screen.' ...
        ' Your job is to remember each shape and its location on the screen.'...
        ' If the object is on the left press the ''a'' key, if on the right press the ''s'' key.' ...
        ' Press the Return key to begin. '];
  
  
  % Display instructions for section
    Screen('FillRect', windowPointer, [0, 0, 0]);
    Screen('TextSize',windowPointer, 30);
    Screen('TextFont',windowPointer, 'Arial');
    Screen('TextStyle', windowPointer, 1);
    DrawFormattedText(windowPointer, instructions, margin,'center',[255 255 255],wrap_at);
    Screen(windowPointer, 'Flip');
    [timeWaited, offsetTime, timedOut] = waitForKeyboard('Return');
    
    
  %% test prepartion for trial 1

    %insert all pictures   
    picturesFID = fopen('List.csv');
    pictures = textscan(picturesFID, '%q%q%q', 'Delimiter', ',');
    picture_list = pictures{1};
    isleft_list = pictures{2};
    is_left = strcmp(isleft_list, '1');
    old_new_list = pictures{3};
    is_new = strcmp(old_new_list, '2');
    fclose(picturesFID);

    trialOrder{1} = [41    37    29    12     8    14    31    24    30    13    15    26    46     7    34    39     2    11    43    16    18    44    42    48    20    17     6    38     9    22     1    32 27    36    25    28     4    45    10    35    47     3    33    19    40    21    23     5];
    trialOrder{2} = [1    16    47    29     5     2    14     8    26    28    10    40    45    12    15    46     6    35    37    20    31    32     4    43    19    17    42    30     7    41    24    21       22    33    36    23    18    27     3     9    13    34    39    38    44    11    48    25];
    trialOrder{3} = [38    13    25    11    12    23    35    22    10    40    37    34    43    16    30    45    14    48    41    15     2    29    33     8    26    27     6     4    36    18     3     1         5    31    46    42    24    47     7    44    32    39    17    19    28    20    21     9];

    %divide pictures into separate lists
    counter = 1;
    for block = 1:3
        % first we grab practice items for each block
        for study_trial = 1:32
            study_pictures{block}{study_trial} = picture_list{counter};
            study_isleft{block}{study_trial} = isleft_list{counter};
            counter = counter + 1;
        end
        
        % then we add some unique items for the study list
        for test_trial = 1:16
            test_pictures{block}{test_trial} = picture_list{counter};
            test_isleft{block}{test_trial} = isleft_list{counter};
            counter = counter + 1;
        end
        % then we combine the practice and unique items for each study list
        test_pictures{block} = [study_pictures{block} test_pictures{block}];
        test_isleft{block} = [study_isleft{block} test_isleft{block}];

        % then we sort the combined list according to our hard-coded trial order (above)
        test_pictures{block} = test_pictures{block}(trialOrder{block});
        test_isleft{block} = test_isleft{block}(trialOrder{block});
    end
    
    %insert one image to determine image size
    img1 = imread('P1E0.jpg');
    
    % allocate positions for the left or the right of the center
    [imgheight, imgwidth, ncolors] = size(img1);
    imgxc = imgwidth/2;
    imgyc = imgheight/2;

    %left position
    left = [xc-imgxc, yc - imgyc, xc-imgxc + size(img1,1), yc - imgyc + size(img1,2)];
    left(1) = left(1) - 50;
    left(3) = left(3) - 50;

    %right position
    right = [xc-imgxc, yc - imgyc, xc-imgxc + size(img1,1), yc - imgyc + size(img1,2)];
    right(1) = right(1) + 50;
    right(3) = right(3) + 50;


  for block = 1:3  
   %% study phase: images are presented on left or right side of screen
 
   % loop over all stimuli
    for trial = 1:length(study_pictures{block});
        
        % set things up based on whether this is a L or R side item
    if strcmp(test_isleft{block}{trial}, '1');
            position = left;
            cresp = allowed_keys_eKleftness(1);
            conditions = 'oldleft';
        else
            position = right;
            cresp = allowed_keys_eKleftness(2);
            conditions = 'oldright';
     end
        
            DrawFormattedText (windowPointer, '+', 'center', 'center', [255 255 255]);
            Screen(windowPointer, 'Flip');
            WaitSecs(.5 * speed);
    
        %Show pictures at their corresponding locations
        this_picture = imread(study_pictures{block}{trial});
        Texture1 = Screen('MakeTexture', windowPointer, this_picture);
        Screen('DrawTexture', windowPointer, Texture1, [], position);
                %add easykeys for eKleftness with duration of 2.5*speed and
                % no user progression (hard set timing)
                          
                % you need to flip before calling easyKeys. If you catch the
                % output, you'll have an exact stimulus onset time that you can feed
                % into easyKeys
                onset = Screen(windowPointer,'Flip');

                % get a response, logging both the stimulus and condition (make
                % sure to catch the result)
                eKleftness = easyKeys(eKleftness, 'onset', onset, 'stim', study_pictures{block}{trial}, ...
                    'cond', conditions, 'cresp', cresp);

     end
    
    
    %% test phase: images are polled as to whether they are new, old_left or old_right
       instructions2 = ['You will now be presented with a variety of pictures. ' ...
           'You will need to press ''a'' if it is an old picture that was presented on the left, ' ...
           ' ''s'' if it was an old picture that was presented on the right, ' ...
           ' and ''d'' if it is a new picture. Press the Return key to begin.'];
    
  % Display instructions for section
    Screen('FillRect', windowPointer, [0, 0, 0]);
    Screen('TextSize',windowPointer, 30);
    Screen('TextFont',windowPointer, 'Arial');
    Screen('TextStyle', windowPointer, 1);
    DrawFormattedText(windowPointer, instructions2, margin,'center',[255 255 255],wrap_at);
    Screen(windowPointer, 'Flip');
    [timeWaited, offsetTime, timedOut] = waitForKeyboard('Return');
    
    % loop over all test stimuli
    for trial = 1:length(test_pictures{block})
        
        % set up cresp based on item type
        if  is_new
            cresp = allowed_keys_eKrecognition(3);
            conditions = 'new';
        elseif  is_left
                cresp = allowed_keys_eKrecognition(1);
                conditions = 'oldleft';
        else
                cresp = allowed_keys_eKrecognition(2);
                conditions = 'oldright';
        end

        %Show pictures at their corresponding locations
        this_picture = imread(test_pictures{block}{trial});
        Texture1 = Screen('MakeTexture', windowPointer, this_picture);
        Screen('DrawTexture', windowPointer, Texture1, [], position);
                %add easykeys for eKrecognition; WaitSecs(2.5 * speed)
                                         
                % you need to flip before calling easyKeys. If you catch the
                % output, you'll have an exact stimulus onset time that you can feed
                % into easyKeys
                onset = Screen(windowPointer,'Flip');

                % get a response, logging both the stimulus and condition (make
                % sure to catch the result)
                eKrecognition = easyKeys(eKrecognition, 'onset', onset, 'stim', test_pictures{block}{trial}, ...
                    'cond', conditions, 'cresp', cresp);

                DrawFormattedText (windowPointer, '+', 'center', 'center', [255 255 255]);
                Screen(windowPointer, 'Flip');
                WaitSecs(.5 * speed);    

        % question 2 regarding confidence
    
        % we will allow two possible responses; other keys 
        % will be ignored. 
         allowed_keys_conf = {'j', 'k'};
         key_labels_conf = {'sure', 'unsure'};
         key_values_conf = [1 2];
       
    
        % we want the computer to code these keyboard values into something we can
        % understand numerically, so we'll give each one a number value
        keymap_conf = makeMap(key_labels_conf, key_values_conf, allowed_keys_conf); 

                keyguide_conf = [];   
                 for j = 1:length(key_labels_conf);
                    keyguide_conf = [keyguide_conf key_labels_conf{j} ': "' allowed_keys_conf{j} '"\n'];
                 end
        
                 DrawFormattedText(windowPointer, ['How confident are you? \n\n' keyguide_conf], 'center', 'center', [255 255 255]);
                      
                % you need to flip before calling easyKeys. If you catch the
                % output, you'll have an exact stimulus onset time that you can feed
                % into easyKeys
                onset = Screen(windowPointer,'Flip');

                % get a response, logging both the stimulus and condition (make
                % sure to catch the result)
                eKconfidence = easyKeys(eKconfidence, 'onset', onset, 'keymap', keymap_conf, ...
                    'cond', conditions);

        DrawFormattedText(windowPointer, '+', 'center', 'center', [255 255 255]);
        Screen(windowPointer, 'Flip');
        WaitSecs(0.5 * speed);

    end
  end 